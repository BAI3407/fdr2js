// 1. Select the div element using the id property
const app = document.getElementById("app");
const viewer = document.createElement("div");
viewer.setAttribute("width", "500px");
viewer.setAttribute("height", "500px");


// 2. Create a new <p></p> element programmatically
const description = document.createElement("ul");
const bucketpath = document.createElement("p");


const userAction = async () => {
   
    // fetch record json
    const response = await fetch('https://www.fdr.uni-hamburg.de/api/records/863');
    const recordjson = await response.json();
    

    // read metadata from json response
    const metadata = recordjson.metadata;
    
    var jsondescription = metadata.description;
    var s = jsondescription.split("<p>");

    var list = s[1].split("<br>")

    list.forEach(element => {
      var listnode = document.createElement("li");
      var listtext = document.createTextNode(element.replace(/(<([^>]+)>)/gi, ""));
      listnode.appendChild(listtext);

      description.appendChild(listnode)
    })
    
    // get bucket path from record json
 
    const bucketresponse = await fetch(recordjson.links.bucket);
    const bucketjson = await bucketresponse.json(); 

    const bucket3dfiles = bucketjson.contents;

    bucket3dfiles.forEach(element =>
    {  
      if(element.key.includes('.glb'))
      {
        const filelink = element.links.self;

        const modelviewer = document.createElement("model-viewer");
        modelviewer.setAttribute("src", filelink);
        modelviewer.setAttribute("auto-rotate", true);
        modelviewer.setAttribute("camera-controls", true);
        viewer.appendChild(modelviewer);
        
      }
    });
}

  userAction();

// 4. Append the p element to the div element
app?.appendChild(description);
app?.appendChild(viewer);
